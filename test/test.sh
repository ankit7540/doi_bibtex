#!/bin/bash

if [ "$#" -ne 1 ]
then
  test_level=0
else
  test_level="$1"
fi

echo ' ----- TEST SCRIPT -----'

../doi_bibTeX
status=$?; echo '  status : ' $status;
if [[ $status -eq 1 ]]; then echo "ok. Expected result."; else echo "Error : Unexpected result";exit 1; fi
echo -e "\t **************************************** "

../doi_bibTeX --
status=$?; echo '  status : ' $status;
if [[ $status -eq 1 ]]; then echo "ok. Expected result."; else echo "Error : Unexpected result";exit 1; fi
echo -e "\t **************************************** "


../doi_bibTeX "10.1063/1.1676771"
status=$?; echo '  status : ' $status
if [[ $status -eq 0 ]]; then echo "ok. Expected result."; else echo "Error : Unexpected result";exit 1; fi

echo -e "\t **************************************** "

../doi_bibTeX -c "10.1007%2Fs002140000204"
status=$?; echo '  status : ' $status
if [[ $status -eq 0 ]]; then echo "ok. Expected result."; else echo "Error : Unexpected result";exit 1; fi

echo -e "\t **************************************** "

../doi_bibTeX -d "https://doi.org/10.1103/PhysRevA.11.1417"
status=$?; echo '  status : ' $status
if [[ $status -eq 0 ]]; then echo "ok. Expected result."; else echo "Error : Unexpected result";exit 1; fi

echo -e "\t **************************************** "

../doi_bibTeX -c  "10.2307/1884513"
status=$?; echo '  status : ' $status
if [[ $status -eq 0 ]]; then echo "ok. Expected result."; else echo "Error : Unexpected result";exit 1; fi

echo -e "\t **************************************** "

../doi_bibTeX -d  "https://doi.org/10.1063/1.2432410"

status=$?; echo '  status : ' $status
if [[ $status -eq 0 ]]; then echo "ok. Expected result."; else echo "Error : Unexpected result";exit 1; fi

echo -e "\t **************************************** \n"

#######################################################
# testing sourced version

if [ "$test_level" -eq 1 ];
then


    echo -e "\t\t Testing sourced version  \n"
    ./source_target_dir

    source ~/.bashrc
    echo -e "\t **************************************** \n"
    doi_bibTeX -h
    if [[ $status -eq 0 ]]; then echo "ok. Expected result."; else echo "Error : Unexpected result";exit 1; fi

    echo -e "\t **************************************** \n"

    doi_bibTeX -d  "https://doi.org/10.1063/1.4756995"
    if [[ $status -eq 0 ]]; then echo "ok. Expected result."; else echo "Error : Unexpected result";exit 1; fi

fi

##########################

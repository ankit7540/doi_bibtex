#!/bin/bash

#target script
target="doi_bibTeX"


# check requirements first #######################

# bash
bash_ver=$(echo "$BASH_VERSION" |  awk -F. '{print $1}'   )

if [ "$bash_ver" -ge 4 ] ; then
    echo -e '\tBash version OK'
else
    echo -e '\tBash version not supported'
    echo 'Exiting...'
    exit;
fi

# ----------------------------

# Python
if [[ "$(python3 -V)" =~ "Python 3" ]]; then
    python_ver=$(python3 -V | awk {'print $2'} | awk -F. {'print $1'})
else
    python_ver=$(python -V | awk {'print $2'} | awk -F. {'print $1'})
fi


if [ -z ${python_ver+x} ]; then
    echo -e "\tPython not found";
    exit;
fi


if [ "$python_ver" -eq 3 ] ; then
    echo -e '\tPython version OK'
else
    echo -e '\tPython version not supported'
    echo 'Exiting...'
    exit;
fi

####################################################
# this script uses .bashrc
# checking if this file exists

if [ ! -f  ~/.bashrc ]; then
    echo -e "\t .bashrc file not found. This script uses bashrc to source the directory."
    echo -e "\tPlease use other ways. ;-) \n\t Exiting."
    exit 0;
fi


####################################################
# get current directory and time stamp
current_dir=$(pwd)
time_stamp=$(date)
####################################################

install_script(){
    echo "" >> ~/.bashrc
    echo '# ---  doi_bibtex script (installed on '"$time_stamp"') --- '  >> ~/.bashrc
    echo 'export PATH='"$current_dir"':$PATH'  >> ~/.bashrc
    echo "#---------------------------" >> ~/.bashrc
}

test_path(){
   if cat ~/.bashrc | grep -q "$current_dir" ; then
      echo "    OK. Done. Do not move or delete this folder. "
      echo -e "    Run the following command to finish. \n\t\t source ~/.bashrc \n"
   else
      echo "    Something went wrong. Check your .bashrc script "
   fi
}

####################################################

if [[ -f "$current_dir/$target" ]];
then
    #echo "This file exists on the filesystem."
    echo "The current directory will be sourced and the main script ($target) will be available globally."

    while true; do
        read -p "Do you want to proceed  (y or n) ?" yn
        case $yn in
            [Yy]* ) echo -e "\n    Installing script..." ; install_script; test_path ; break;;
            [Nn]* ) echo 'Exiting.';exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done
else
    echo "Target script $target not found in the current directory ! Exiting"
fi

####################################################


# doi_bibtex

  
[![pipeline status](https://gitlab.com/ankit7540/doi_bibtex/badges/main/pipeline.svg)](https://gitlab.com/ankit7540/doi_bibtex/-/commits/main)  ![](https://img.shields.io/badge/version-1.0-blue)
 
**bash script for obtaining bibtex reference for a doi**

A terminal based tool with core functionality based on the [requests](https://docs.python-requests.org/en/latest/)  module in python.

**Table of Contents**

[TOC]

---

##### Requirements
                
+ `bash`
+ `python3`
    + `requests`


##### Tested with

+ `bash 4.4+`
+ `python 3.5+`
    + `requests 2.2+`


-----


### Usage

```
doi_bibTex : a bibtex parser for bash

Usage: ./doi_bibTeX  <doi string or full doi url>

   Optional flags:
                  [-c (condense journal name)] <doi string or full doi url>
                  [-d (condense journal name with dot)] <doi string or full doi url>
                  [-h (help)]
Provide DOI in double quotes if DOI string has special characters like brackets or the % symbol.

```

### Standard installation

```
git clone https://gitlab.com/ankit7540/doi_bibtex

```
If global access to the main script is needed, source the directory using the script `source_this_dir.sh`

-----

### Examples

```
./doi_bibTeX  https://doi.org/10.1093/cje/27.2.243


 @article{Laursen_2003,
        doi = {10.1093/cje/27.2.243},
        url = {https://doi.org/10.1093%2Fcje%2F27.2.243},
        year = 2003,
        month = {mar},
        publisher = {Oxford University Press ({OUP})},
        volume = {27},
        number = {2},
        pages = {243--263},
        author = {K. Laursen},
        title = {New human resource management practices, complementarities and the impact on innovation performance},
        journal = {Cambridge Journal of Economics}
}

```

#### With optional flags

##### with -c flag 

```
./doi_bibTeX -c  10.1063/1.1744445


@article{Miyazawa_1958,
        doi = {10.1063/1.1744445},
        url = {https://doi.org/10.1063%2F1.1744445},
        year = 1958,
        month = {jul},
        publisher = {{AIP} Publishing},
        volume = {29},
        number = {1},
        pages = {246--246},
        author = {Tatsuo Miyazawa},
        title = {Symmetrization of Secular Determinant for Normal Vibration Calculation},
        journal={J Chem Phys}
}

```

##### with -d flag

```
./doi_bibTeX -d "10.1016/S0022-2836(60)80022-8"


@article{Uchida_1960,
        doi = {10.1016/s0022-2836(60)80022-8},
        url = {https://doi.org/10.1016%2Fs0022-2836%2860%2980022-8},
        year = 1960,
        month = {nov},
        publisher = {Elsevier {BV}},
        volume = {2},
        number = {5},
        pages = {262--272},
        author = {Hisao Uchida and Gunther S. Stent},
        title = {Protein synthesis and the onset of intracellular bacteriophage growth},
        journal={J. Mol. Biol.}
}

```

#### Processing list of DOIs in a file to generate bib file

Iterating over a file contaning DOI strings (one per each line) in a file is easily possible using standard bash tools. For example, a file named as `doi_list` having a number of  DOI urls (or strings), one per line can be processed using the command, 

`cat doi_list | while read -r line; do if [ -n "$line" ]; then  doi_bibTeX "$line" >> ref.bib ; fi  ; done ; `

This will read each line and pass each line to the `doi_bibtex` program and return the output to `ref.bib` file.

### Journal name shortening (using `-c` or `-d` flags) and possible issues

This functionality is based on a database of journal names (containing over 80, 000 entries). Alistair G Auffret is acknowledged for [providing the list on figshare](https://su.figshare.com/articles/dataset/Journal_abbreviations_from_Web_of_Science/3207787 "providing the list in figshare"). 

- If your doi is valid, then the program will output the bibtex entry, without the `-c` or `-d` flag. 

- However, if a valid journal name  is not available in the database, the program will give an error ```"Err : No matching journal found!"```, when using the `-c` or `-d` flag. **In this case, one can simply type the shortend name manually, or add the entry to the database file. See `dep/wos_abbrev_table.csv` file.**  The standard format is :

```
"<full journal name>";"short journal name";"short journal name with dots"
```
For example,

```
"Accounts of Chemical Research";"Acc Chem Res";"Acc. Chem. Res." 
```

---

#!/bin/bash

path=$(dirname "$0")

arg=""
d=0 # this is the second option for condensed mode

####################################################
# set python binary
command -v python3 >/dev/null && PYTHON_BIN=python3 || PYTHON_BIN=python

#which $PYTHON_BIN
####################################################
# defining functions below

usage() {            # Function: Print a help message
  echo -e "\n\tdoi_bibTex : a bibtex parser for linux."
  echo -e "\tUsage: doi_bibTex  [-c (condense journal name)] [-d (condense journal name with dot)] <doi string or url>" 1>&2
  echo -e "\t    Provide DOI in double quotes if the DOI string \n\t    has special characters like brackets or the % symbol."
}

exit_abnormal() {    # Function: Exit with message
  usage
  exit 1
}

help(){
  echo -e "\n\tdoi_bibTex : a bibtex parser for linux. "
  echo -e "\tUsage: doi_bibTex <doi_string or url>"
  echo -e "\t       doi_bibTex  [-c (condense journal name)] <doi_string or url>"
  echo -e "\t       doi_bibTex  [-d (condense journal name with dot)] <doi string or url>" 1>&2
  echo -e "\t       doi_bibTex  [-h (this help message)]"
  echo -e "\t    Provide DOI in double quotes if the DOI string \n\t    has special characters like brackets or the % symbol.\n"
  exit 0
}

bib_parser_basic(){       # Function: process request
   $PYTHON_BIN -c "import sys; sys.path.append('$path/dep'); import core; core.doi_to_bib('$arg')"
}

bib_parser_condensed(){       # Function: process request
   $PYTHON_BIN -c "import sys; sys.path.append('$path/dep'); import core; core.doi_to_bib_condensed('$arg', '$d')"
}

####################################################
# check for passed arguments

if [ $# -eq 0 ];
  then
    echo "    Error : No arguments supplied"
    exit_abnormal
fi

######################################
if [ $1 == "" ]; then
  echo " Error: No argument provided"
  exit_abnormal
fi


if [ -z  "$1" ]; then
  echo " Error. No argument provided"
  exit_abnormal
fi

if [ $1 = "--" ]; then
    echo " Error: -- as argument is not accepted"
    exit_abnormal
fi


####################################################
caution(){
       size=${#arg}

       if [ $size -gt 5 ]; then
           bib_parser_basic                        # try with basic run
       else
           echo "Error : doi string too short or unknown flag. Aborting.";
           exit_abnormal
       fi
}

####################################################


while getopts ":hc:d:" options; do         # Loop: Get the next option;
                                          # use silent error checking;

  case "${options}" in                    #
    h)
       help
      ;;

    c)                                    # If the option is c
      arg=${OPTARG}
      bib_parser_condensed
      ;;
    d)                                    # If the option is d
      arg=${OPTARG}
      d=1
      bib_parser_condensed
      ;;
    :)                                    # If expected doi string omitted:
      echo "Error: <DOI> missing -${OPTARG} requires an argument."
      exit_abnormal                       # Exit abnormally.
      ;;
    *)                                    # If unknown (any other) option:
       #echo $arg
       arg=${OPTARG}
       echo ' Note : unknown flag. Trying...'
       caution
      ;;
  esac
done

##################################################

if [ "$arg" = "" ]; then                 # If $arg is an empty string,
  if [ -z  "$1" ]; then
    echo " No argument provided"
  elif [ "$arg" = "--" ]; then
    exit_abnormal
  else
    arg="$1"
    bib_parser_basic
  fi
fi

##################################################

exit 0                                    # Exit normally.

import sys
import requests

from pathlib import Path
path = Path(__file__).parent.absolute() # for system specific file database file path

#################################################

def doi_to_bib(idvalue):
    '''
    idvalue : doi as string ( with quotes is safer)

    '''

    if "doi.org" in idvalue:
        idvalue=idvalue.split("doi.org/")[1]

    base = "https://dx.doi.org/"
    url = base + idvalue
    url = url.strip()


    #print("\t",url)

    resp = requests.get(url,
                        headers={'Accept':'application/x-bibtex'})

    if resp.status_code == 200:
        print("\n\n", resp.text, "\n\n")
    else:
        print("\t Error (100): DOI search failed. Check DOI string OR try providing DOI in quotes.")

#################################################
#################################################

def file_searcher(filepath, term, condense_mode):

    '''
      filepath       : database file path
      term           : search term (journal name in this case)
      condense_mode  : 0 condense mode
                       1 condense mode with dot
    '''
    condensed_mode = int(condense_mode)
    file = open( filepath, "r") # open file in read mode

    found=0  # variable used to test successful search
    for line in file:
        if term in line:

            split_data = line.split(";")
            search_string=split_data[0].replace('"','')
            search_string=search_string.strip()
            #print('search string:', search_string)


            if ( term in search_string and term == search_string ) :
                found=1

                db_out = line

                mode1 = split_data[1].replace('"', '')
                mode2 = split_data[2].replace('"', '')

                mode1 = mode1.replace('\n', '')
                mode2 = mode2.replace('\n', '')

                journalName=''

                if (condense_mode == 0) :
                    journalName=mode1
                elif (condense_mode == 1) :
                    journalName=mode2

                #print('Test :', found, journalName)

                if (found == 1 and journalName != '' ):
                    return 0, journalName
                else:
                    return 1, ' Error (101) : Exact entry not found in database'

    if (found == 0):
        return 1, 'Error (101) : No matching journal found!'

#################################################

def get_short_JournalName(out, condense_mode):

    """
    out            : string from the doi.org website
    condense_mode  : 0 or 1
                     0 = condense mode
                     1 = condense mode with dot
    """

    condense_mode=int(condense_mode)

    if '@article{' in out:   # do search only for articles
        for item in out.split("\n"):
            if "journal" in item:
                line=(item.strip())

                # getting journal name
                jname=line.split("=")[1].strip()
                jname=jname.replace("{", "")
                jname=jname.replace("}", "")
                jname=jname.replace("\\", "")  # some journal names have backslashes to escape symbols

                #print('journal name : ', jname)

                dbfile=str(path)+'/wos_abbrev_table.csv'

                # search in database file
                result = file_searcher(dbfile, jname, condense_mode)

                if (result[0] == 0):
                    return result
                elif ( 'the' in jname.lower() ):

                    # remove 'The' from the journal name and try search again
                    jname_c=jname.strip('The ')
                    jname=jname_c.strip()

                    result = file_searcher(dbfile, jname, condense_mode)

                    if (result[0] == 0):
                        return result
                    else:
                        return 1, 'Error (103) : Journal not found in the database!'

                else:
                    return 1, 'Error (105) : Journal not found in the database!'

#################################################

def doi_to_bib_condensed(idvalue, cmode):
    '''
    idvalue : doi as string in quotes
    cmode   : condensed mode
    '''

    if "doi.org" in idvalue:
        idvalue=idvalue.split("doi.org/")[1]

    base = "https://dx.doi.org/"
    url = base + idvalue
    url=url.strip()
    #print("\t",url)

    resp = requests.get(url,
                        headers={'Accept':'application/x-bibtex'})

    if resp.status_code == 200:

        # perform post processing
        out = resp.text

        # get the (return code, shorter name of the Journal)
        check = get_short_JournalName(out, cmode)

        new_output=''

        if (check[0] == 0) :
            subs = check[1]

            caps = list(filter(str.isupper, subs.title()))
            abbrv = ''.join(map(str, caps))
            #print(' Abbreviated title:', abbrv)

            # do replacement here
            for item in out.split("\n"):
                #print(item)
                if "journal" in item:
                    line=(item.strip())
                    if "}," in line:
                        new_line='        journal={'+subs+'},'
                    else:
                        new_line='        journal={'+subs+'}'
                    #print('new line : ', new_line)
                    item=new_line
                new_output=new_output+'\n'+item

            print(new_output)
            print('\n\n')

        else:
            print('\t', check[1])

    else:
        print("\t Error (101) : DOI search failed. Check DOI string OR try providing DOI in quotes.")

#################################################
